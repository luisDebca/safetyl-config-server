FROM adoptopenjdk/openjdk11:jdk-11.0.6_10-alpine-slim
ARG JAR_FILE=target/safetyl-config-server.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]